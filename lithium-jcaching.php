<?php
/**
 * Plugin Name: _jCaching
 * Plugin URI: http://about:Blank
 * Description: Enqueues jQuery with selector caching to improve performance. Just use $$ instead of $. This also adds the cookie.js library.
 * Version: 1.0.0
 * Author: Lithium Sixteen
 * Author URI: http://about:Blank
 */
 
 
 // Enqueue public scripts
function lithium_sjC_ncww_enqueue_public_js() {
    
    wp_register_script( 'lithium_jQuery', plugin_dir_url(__FILE__) . 'js/jquery-3.3.1.min.js', false, false, true );
	wp_enqueue_script( 'lithium_jQuery' );

	wp_register_script( 'lithium_jcaching', plugin_dir_url(__FILE__) . 'js/lithium-jcaching.js', array( 'lithium_jQuery' ), false, false );
	wp_enqueue_script( 'lithium_jcaching' );

}
add_action( 'wp_enqueue_scripts', 'lithium_sjC_ncww_enqueue_public_js' );